/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.chanatip.swingproject;

import java.io.Serializable;

/**
 *
 * @author chanatip
 */
public class Friend implements Serializable{
    private String Name;
    private int age;
    private String gender;
    private String description;

    public Friend(String Name, int age, String gender, String description) {
        this.Name = Name;
        this.age = age;
        this.gender = gender;
        this.description = description;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Friend{" + "Name=" + Name + ", age=" + age + ", gender=" + gender + ", description=" + description + '}';
    }
    
}
